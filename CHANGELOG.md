# Changelog

## [Unreleased]

## [0.2.0] - 2023-09-17

* Move initial blocks to legacy wile working in refactoization and
  standarization.
* Fix wagtail-sb-imageserializer dependency version.
* Update development dependencies.

## [0.1.0] - 2023-09-04

* Add base clases.
* Configure linting tools.
* Configure base tests.
