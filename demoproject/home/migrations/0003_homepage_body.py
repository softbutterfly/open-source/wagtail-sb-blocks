# Generated by Django 4.2.4 on 2023-09-04 09:37

from django.db import migrations
import wagtail.blocks
import wagtail.documents.blocks
import wagtail.embeds.blocks
import wagtail.fields
import wagtail.images.blocks
import wagtail_sb_blocks.blocks


class Migration(migrations.Migration):
    dependencies = [
        ("home", "0002_create_homepage"),
    ]

    operations = [
        migrations.AddField(
            model_name="homepage",
            name="body",
            field=wagtail.fields.StreamField(
                [
                    ("h1", wagtail_sb_blocks.blocks.legacy.H1()),
                    ("h2", wagtail_sb_blocks.blocks.legacy.H2()),
                    ("h3", wagtail_sb_blocks.blocks.legacy.H3()),
                    ("h4", wagtail_sb_blocks.blocks.legacy.H4()),
                    ("h5", wagtail_sb_blocks.blocks.legacy.H5()),
                    ("h6", wagtail_sb_blocks.blocks.legacy.H6()),
                    ("p", wagtail_sb_blocks.blocks.legacy.Paragraph()),
                    (
                        "quote",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "text",
                                    wagtail_sb_blocks.blocks.legacy.Paragraph(
                                        label="Quote"
                                    ),
                                ),
                                (
                                    "author",
                                    wagtail.blocks.CharBlock(
                                        label="Author", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "figure",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "images",
                                    wagtail.blocks.ListBlock(
                                        wagtail.images.blocks.ImageChooserBlock(
                                            labe="Images"
                                        )
                                    ),
                                ),
                                (
                                    "caption",
                                    wagtail_sb_blocks.blocks.legacy.Paragraph(
                                        label="Legend", required=False
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "ordered_list",
                        wagtail_sb_blocks.blocks.legacy.OrderedList(
                            wagtail_sb_blocks.blocks.legacy.Paragraph(label="Item")
                        ),
                    ),
                    (
                        "unordered_list",
                        wagtail_sb_blocks.blocks.legacy.UnorderedList(
                            wagtail_sb_blocks.blocks.legacy.Paragraph(label="Item")
                        ),
                    ),
                    (
                        "embed",
                        wagtail.blocks.StructBlock(
                            [
                                (
                                    "embed_object",
                                    wagtail.embeds.blocks.EmbedBlock(
                                        form_classname="embed-inner-wrapper",
                                        help_text="Url's to embed content from other sites compatible with oembed.",
                                        label="URL",
                                        required=False,
                                    ),
                                ),
                                (
                                    "embed_raw",
                                    wagtail.blocks.RawHTMLBlock(
                                        help_text="Insert snippet to embed content from other sites.",
                                        label="HTML",
                                        required=False,
                                    ),
                                ),
                                (
                                    "caption",
                                    wagtail_sb_blocks.blocks.legacy.Paragraph(
                                        label="Legend", required=False
                                    ),
                                ),
                                (
                                    "embed_ratio",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            (None, "Without ratio"),
                                            ("ratio-16-9", "16:9"),
                                            ("ratio-4-3", "4:3"),
                                        ],
                                        label="Aspect ratio",
                                        required=False,
                                    ),
                                ),
                            ]
                        ),
                    ),
                    (
                        "button",
                        wagtail.blocks.StructBlock(
                            [
                                ("text", wagtail.blocks.CharBlock(label="Text")),
                                (
                                    "link",
                                    wagtail.blocks.URLBlock(
                                        label="External link", required=False
                                    ),
                                ),
                                (
                                    "page",
                                    wagtail.blocks.PageChooserBlock(
                                        label="Link to page", required=False
                                    ),
                                ),
                                (
                                    "document",
                                    wagtail.documents.blocks.DocumentChooserBlock(
                                        label="Link to document", required=False
                                    ),
                                ),
                                (
                                    "size",
                                    wagtail.blocks.ChoiceBlock(
                                        choices=[
                                            (None, "Normal"),
                                            ("small", "Small"),
                                            ("large", "Large"),
                                        ],
                                        label="Size",
                                        required=False,
                                    ),
                                ),
                                (
                                    "custom_id",
                                    wagtail.blocks.CharBlock(
                                        label="Custom ID",
                                        max_length=255,
                                        required=False,
                                    ),
                                ),
                                (
                                    "tracking_event_category",
                                    wagtail.blocks.CharBlock(
                                        label="Tracking Event Category",
                                        max_length=255,
                                        required=False,
                                    ),
                                ),
                                (
                                    "tracking_event_label",
                                    wagtail.blocks.CharBlock(
                                        label="Tracking Event Label",
                                        max_length=255,
                                        required=False,
                                    ),
                                ),
                            ]
                        ),
                    ),
                    ("html", wagtail_sb_blocks.blocks.legacy.HTMLBlock()),
                ],
                blank=True,
                null=True,
                use_json_field=True,
                verbose_name="body",
            ),
        ),
    ]
