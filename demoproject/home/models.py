from django.utils.translation import gettext_lazy as _

from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail.models import Page

from wagtail_sb_blocks.blocks.legacy import (
    Button,
    Embed,
    Figure,
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    HTMLBlock,
    OrderedList,
    Paragraph,
    Quote,
    StreamBlock,
    UnorderedList,
)


class HomeContent(StreamBlock):
    h1 = H1()
    h2 = H2()
    h3 = H3()
    h4 = H4()
    h5 = H5()
    h6 = H6()
    p = Paragraph()
    quote = Quote()
    figure = Figure()
    ordered_list = OrderedList(
        Paragraph(
            label=_("Item"),
        ),
    )
    unordered_list = UnorderedList(
        Paragraph(
            label=_("Item"),
        ),
    )
    embed = Embed()
    button = Button()
    html = HTMLBlock()

    class Meta:
        label = _("Content")
        icon = "doc-full"


class HomePage(Page):
    body = StreamField(
        HomeContent(),
        verbose_name=_("body"),
        blank=True,
        null=True,
        use_json_field=True,
    )

    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]
