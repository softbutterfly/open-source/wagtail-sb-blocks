from django.apps import AppConfig


class WagtailSbBlocksConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "wagtail_sb_blocks"
